# react-hooks

> Various react hooks

[![NPM](https://img.shields.io/npm/v/@karsegard/react-hooks.svg)](https://www.npmjs.com/package/@karsegard/react-hooks)

## Install

```bash
npm install --save @karsegard/react-hooks
```

## Usage

  see [example](example)

## License

MIT © [FDT2k](https://gitlab.com/FDT2k) [Karsegard Digital Agency Sàrl](https://www.karsegard.ch)
