import React,{ useState,useEffect } from "react";

import useWebWorker from './useWebWorker';


/*
    For any function

    args => {

        return result
    }





*/

export default (fn)=>{

    const [result, setResult] = useState();


    const workerFn = fn=> _=> {
        onmessage=event=>{

            postMessage(fn(event.data));
        }
    }
    const _worker = useWebWorker(workerFn(fn));


    useEffect(()=>{
        if(_worker && !result){


            setResult(args=>{
                _worker.postMessage(args);
                return new Promise((resolve,reject)=>{

                    _worker.onmessage=resolve;
                    _worker.onmessageerror=reject;
                    _worker.onerror=reject;

                });
            });
        }

    },[_worker]);


    return  result
}
