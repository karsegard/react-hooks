import React,{ useEffect,useRef } from "react";
/**
* Given a workPath and input, and some state callbacks, run a worker and track its progress
*
* @param workerPath {string} Path to a web worker @see https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API
* @param input {any} The input posted initially to the web worker
* @param setVal {Function} The function to call with the result of the web worker
* @param setProgress {Function} The function to call with the progress of the web worker
*
* @returns effect clean up callback
*/
export default (workerFunction,callback) => {
    const workerRef = useRef();

    useEffect(
        () => {
            const worker = new workerFunction();
            workerRef.current = worker;
            worker.onmessage = message => callback(message.data)
            return () => {
                worker.terminate();
            };
        },
        [workerFunction]
    );

    return [
        (...args) => {
            workerRef.current.postMessage(...args);
        }
    ];
};
