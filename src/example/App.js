import React, { useEffect, useRef, useState } from 'react'
import { useFieldValues, useWorkerFunction, useFocus, useKeypress, useForm, useDragList } from '@'
import { Container, Fullscreen, LayoutFlex, LayoutFlexColumn } from '@karsegard/react-core-layout'
import '@karsegard/react-core-layout/dist/index.css'
import '@/example/index.css'
import { is_nil } from '@karsegard/composite-js'


const randomColor = _ => "#" + ((1 << 24) * Math.random() | 0).toString(16);

const RenderCount = () => {
  const renderCounter = useRef(0);
  renderCounter.current = renderCounter.current + 1
  return <>{renderCounter.current}</>
}


const Tile = props => {

  return (

    <Container cover style={{ backgroundColor: randomColor() }}>
      <LayoutFlexColumn cover justCenter alignCenter>
        {props.children}
      </LayoutFlexColumn>
    </Container>
  )
}

const Intro = () => {


  return (
    <Tile>
      <h1>Intro</h1>
      <p>This app demonstrate the different hooks provided by this package, just scroll for more</p>
    </Tile>
  )
}

const TestUseKeyPress = () => {


  const isKeyPressed = useKeypress(['p', 'P'])
  const isModifierPressed = useKeypress(['Shift', 'Enter'])

  const [enabled, setEnabled] = useState(false)

  const anotherKeyPress = useKeypress(['r'], enabled);

  useEffect(() => {
    if (isKeyPressed) {
      setEnabled(true)
    } else {
      setEnabled(false)

    }

  }, [isKeyPressed])

  return (
    <Tile>
      <RenderCount />
      <h1>useKeyPress</h1>
      <p>This hooks allow you to track pressed keys</p>
      <p>
        {!isKeyPressed && <>Press the p key</>}
        {isKeyPressed && <> You pressed the p key</>}
        <br />
        {!isModifierPressed && <>Press Shift or Enter key</>}
        {isModifierPressed && <>MODIFIER ENABLED</>}
        <br />

        {anotherKeyPress && "Yes !!"}
        {!anotherKeyPress && "Press p to enable this hook and then r"}


      </p>
    </Tile>
  )
}

const TestFormHooks = () => {

  const _validate = (name, value, values) => {
    if (name === "name") {
      if (value == "") {
        return 'name is mandatory'
      }
      if (value.length < 10) {
        return 'at least 10 chars'
      }
    }

    if (name == 'home' && value !== true) {
      return "you have to accept the conditions"
    }
    return true;
  }


  const handleValueChange = (values,key) => {
    console.log('values changed', values)
    console.log('changed key',key)

  }

  const { fields, inputProps, formProps, validator, replaceValues, fieldValidation, assignValues, handleChangeValue } = useForm({ name: '', changevalue: 'prout', home: false }, { onSubmit: values => { alert('form submitted') }, validate: _validate, onValuesChange: handleValueChange, usePath: true });
  return (
    <Tile>
      <RenderCount />
      <h1>Hook Forms</h1>
      <pre>{JSON.stringify(validator, null, 10)}</pre>
      <form {...formProps}>
        <LayoutFlexColumn>

          <input type="text" name="name" {...inputProps('name')} />
          {fieldValidation('name', ({ error, touched, valid }) => {

            return (<>
              {!valid && touched && <span className="error">{error}</span>}
            </>)
          })}

          <LayoutFlex><input type="checkbox" name="home" checked={fields.home}{...inputProps('home')} /> <div>{fieldValidation('home', ({ error, touched, valid }) => {

            return (<>
              {!valid && touched && <span className="error">{error}</span>}
            </>)
          })}</div></LayoutFlex>


          <LayoutFlex> <input type="text" name="changevalue" value={fields['changevalue']} onChange={e => {
            handleChangeValue('changevalue', e.target.value)
          }} /></LayoutFlex>
          <LayoutFlex> <input type="text" {...inputProps('groups.test')} /></LayoutFlex>

          <button onClick={e => { replaceValues({ name: 'FabienKarsegard', home: true }); e.preventDefault() }}>Programmaticaly fill</button>
          <button type="submit">test</button>
        </LayoutFlexColumn>
      </form>
    </Tile>
  )
}


const TestA = () => {

  const handleFormChange = values => {
    console.log('values changed, this shounldnot trigger a rerender');

    replaceValues(state => {
      return {
        ...state,
        field2: 'hey'
      }
    })
  }
  const { values, replaceValues, inputProps, handleChange, handleChangeValue } = useFieldValues({ field: 'hello world', field2: 'hihi' }, { onValuesChange: handleFormChange })

  const ref = useRef();
  const { hasFocus } = useFocus({ ref });

  const enterPressed = useKeypress('Enter');

  const [firstworker] = useWorkerFunction({
    fn: (e) => {
      postMessage('hello world');
    },
    callback: reply => {
      console.log('first worker replied')
    }
  }
  );


  console.log('render');
  const testworker = _ => {
    firstworker('hey')
  }


  console.log('inputProps', inputProps('test'))


  return (
    <Tile>
      <h1>useWorkerFunction</h1>
      <div>
        <div>
          {values.field}<br />
          {hasFocus && <div>Press Enter to win !</div>}
          {hasFocus && enterPressed && <div>You won</div>}
          <input type="text" ref={ref} name="field" onChange={handleChange} value={values.field} />
        </div>

        <div>
          {values.field}<br />
          <input type="text" {...inputProps('field')} />
        </div>
        <div>
          {values.field2}<br />
          <input type="text" name="field2" {...inputProps('field2')} />
        </div>

        <div>
          {values.field}<br />
          <input type="text" name="field" value={values.field} onChange={e => handleChangeValue('field')(e.target.value)} />
        </div>
        <button onClick={testworker}> worker ?</button>
      </div>
    </Tile>
  )
}

const TestUseDragList = () => {

  const [list, setList] = useState([
    { label: 'Red', id: 0 },
    { label: 'Yellow', id: 1 },
    { label: 'Green', id: 2 },
    { label: 'Color', id: 3 },
    { label: 'Blob', id: 4 },
  ])

  const { dragged, over, dragStart, dragEnd, onDragOver, itemProps } = useDragList({
    onDrop: (from, to) => {
      console.log(list, from, to)
      const a = list.findIndex(item => from.dataset.id == item.id);
      const b = list.findIndex(item => to.dataset.id == item.id);
      if (a > -1 && b > -1) {
        const newlist = list.reduce((carry, item, idx) => {
          if (idx === a) {
            return carry;
          } else if (idx === b) {
            carry.push(list[a]);
            carry.push(item);
          } else {
            carry.push(item)
          }
          return carry;
        }, [])

        setList(newlist)
      }
    }
  })


  return (
    <Tile>
      <RenderCount />
      <h1>useDragList</h1>
      <div onDragOver={onDragOver}>
        <ul>
          {list.map((item, key) => {
            return (<li data-id={item.id} key={key} {...itemProps}>{item.label}</li>)

          })}

        </ul>
        <br />
        <br />
        <br />
        <br />
        <br />
        <LayoutFlex className="horizontal-drag">
          {list.map((item, key) => {
            return (<div data-id={item.id} key={key} {...itemProps}>{item.label}</div>)

          })}

        </LayoutFlex>

      </div>
    </Tile>
  )
}

const App = props => {

  return (
    <Fullscreen overflowY>
      <Intro />
      <TestUseDragList />
      <TestUseKeyPress />
      <TestFormHooks />
      <TestA />
    </Fullscreen>
  )
}



export default App
