import React, { useState, useEffect } from "react";

import useFieldValues from '@/useFieldValues'
import useFieldValidator from '@/useFieldValidator'

export default (initialState, { onSubmit, validate, attribute = 'name', usePath = false, dbg = true, onValuesChange } = {}) => {

  const [debug, setDebug] = useState(dbg || false)
  const [formTouched, setFormTouched] = useState(false)
  const [shouldSubmit, setShouldSubmit] = useState(false);
  const { values: fields, replaceValues, handleChange: handleFieldChange, inputProps, assignValues, handleChangeValue } = useFieldValues(initialState, { attribute, usePath, onValuesChange });

  const { formValid, validationStatus, errors, handleValidation, setTouched, setDebug: setValidatorDebug } = useFieldValidator(initialState, validate)


  const handleSubmit = (event) => {

    submit();
    event.preventDefault();
  }

  const submit = _ => {
    setFormTouched(true)
    debug && console.log('[SUBMIT_ATTEMPT]', formValid, fields, validationStatus)
    handleValidation(fields, true);

    setShouldSubmit(true)
  }

  const handleEvent = (event) => {
    const fieldKey = event.target[prop]
    debug && console.log('[FORM_EVENT]', event.type, event.target[prop])
    if (event.type === 'input') {
      let newState = handleFieldChange(event);
      //    handleValidation(newState)
    }

    if (event.type === 'blur') {
      setTouched(fieldKey, true)

      handleValidation(fields)
    }
    if (event.type === 'focus') {


    }
  }

  useEffect(() => {
    if (shouldSubmit && formValid) {
      debug && console.log('[FORM IS VALID -> SUBMITTING]', fields)
      debug && !onSubmit && console.warn('no submit function')
      onSubmit && onSubmit(fields, validationStatus);
    }
    if (shouldSubmit) {
      setShouldSubmit(false)
    }
  }, [shouldSubmit, formValid])

  useEffect(() => {
    setValidatorDebug(debug)

    handleValidation(fields)
  }, [])

  return {
    fields,
    setDebug,
    validator: validationStatus,
    handleSubmit,
    submit,
    formTouched,
    fieldValidation: (key, render) => {
      return render(validationStatus[key] || {})
    },
    formProps: {
      onSubmit: handleSubmit
    },
    handleInput: handleEvent,
    handleEvents: { onChange: handleEvent, onBlur: handleEvent, onFocus: handleEvent },
    // useFieldValue
    assignValues,
    inputProps,
    handleChangeValue,
    replaceValues,
    
  };
}
