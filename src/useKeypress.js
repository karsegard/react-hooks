import React, { useState, useEffect, useRef } from 'react';
import { is_array } from '@karsegard/composite-js'

export default (targetKey, enabled = true) => {
  const [keyPressed, setKeyPressed] = useState(false);

  const targets = useRef();


  function downHandler({ key }) {
    if (is_array(targets.current)) {
      if (targets.current.includes(key)) {
        setKeyPressed(true);
      }

    }

  }

  const upHandler = ({ key }) => {
    if (is_array(targets.current)) {
      if (targets.current.includes(key)) {
        setKeyPressed(false);
      }

    }
  };

  useEffect(() => {
    if (enabled) {
      if (!is_array(targetKey)) {
        targets.current = [targetKey];

      } else {
        targets.current = targetKey;
      }

      window.addEventListener("keydown", downHandler);
      window.addEventListener("keyup", upHandler);
      return () => {
        window.removeEventListener("keydown", downHandler);
        window.removeEventListener("keyup", upHandler);
      };
    }else{
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
    }
  }, [enabled]);
  return keyPressed;
}