import React, { useState, useMemo, useCallback, useEffect } from "react";
import {safe_path,as_safe_path, is_nil} from '@karsegard/composite-js'
import {curry} from '@karsegard/composite-js/Curry'

const default_opts = {
  attribute: 'name',
  onValuesChange: x => x,
  usePath: false
}

export default (initialState = {}, opts) => {


  const [values, setValues] = useState(initialState);
  const [touched, setTouched] = useState(false);

  const { attribute, onValuesChange,handleValuesChange, usePath } = Object.assign({}, default_opts,opts);

  useEffect(()=> {
    if(!is_nil(handleValuesChange)){
      console.error('[useFieldValues] handleValuesChange is deprecated use onValuesChange instead')
    }
  },[])
 
  const handleChange = (event) => {
    setTouched(true);

    if (!event || !event.target) {
      return;
    }
    if (typeof (event.target[attribute]) === 'undefined') {
      throw new Error(`[useFieldValue] attribute "${attribute}"  not present on target node`)
    }

    let newState = values;
    let new_value = event.target.type === 'checkbox' ? event.target.checked : event.target.value

    const key = event.target[attribute];
    if (!usePath) {
      newState = {
        ...values,
        [key]: new_value

      }
    } else {
      newState = as_safe_path(key,newState,new_value);
    }

    setValues(newState);
    onValuesChange && onValuesChange(newState,key);
    return newState;

  }



  const handleValue = curry((field,value) => {
    setTouched(true);
    const newState = {
      ...values,
      [field]: value
    }
    setValues(newState);
    onValuesChange && onValuesChange(newState,field);
    return newState;
  },true)

  const replaceValues = setValues

  const assignValues = (vals) => {
    setValues(state => ({
      ...state,
      ...vals
    }))
  }

  const getValues = prop => {

    if(usePath){
      return safe_path('',prop,values)
    }else {
      return values[prop];
    }
  }

  return {
    values,
    touched,
    handleChange,
    handleChangeValue: handleValue,
    replaceValues,
    assignValues,

    inputProps: prop => ({
      onChange: handleChange,
      value: getValues(prop),
      [attribute]: prop
    })
  };
}
