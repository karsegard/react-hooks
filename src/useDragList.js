import React, { useEffect, useRef } from 'react';

export const placeholder = document.createElement("li");
placeholder.className = "placeholder";

const defaultDropEvent = (x, y) => console.warn('you didn\'t set a onDrop event ', x, y)

export default (args = { onDrop: defaultDropEvent }) => {

    const dragged = useRef();
    const over = useRef();
    const { onDrop: dropEvent } = args;
    const onDrop = useRef(dropEvent);


    function onDragStart(e) {
        dragged.current = e.currentTarget;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', dragged.current);
    }

    function onDragEnd(e) {
        dragged.current.style.display = 'block';
        placeholder.parentNode.removeChild(placeholder);
//        dragged.current.parentNode.removeChild(placeholder);


        var from = dragged.current;
        var to = over.current;
        dropEvent && dropEvent(from, to);

    }

    function onDragOver(e) {
        e.preventDefault();
        dragged.current.style.display = "none";
        if (e.target.className === 'placeholder') return;
        over.current = e.target;
        e.target.parentNode.insertBefore(placeholder, e.target);
    }


    return {
        dragged, over, onDragStart, onDragEnd, onDragOver, itemProps: {
            onDragStart,
            onDragEnd,
            draggable: 'true'
        }
    };
}