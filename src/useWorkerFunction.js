import React,{ useState,useEffect,useRef } from "react";


export default (args)=>{

    const defaultArgs = {appendOnMessage:true, callback:x=>x}

    const {fn, appendOnMessage,callback} = Object.assign({},defaultArgs,args);

    const [blob,setBlob] = useState();
    const [workerSupported,setWorkerSupported]= useState(false);

    const workerRef = useRef();

    useEffect(()=>{
        setWorkerSupported( typeof window.Worker !== "undefined");

        let f = fn.toString();
        if(appendOnMessage){
            f = '_=>{onmessage='+f+'}';
        }
        console.log(f);
        setBlob(new Blob(['(',f, ')();'],{type: 'text/javascript'}));
    },[])

    useEffect(()=>{
        if(workerSupported && !workerRef.current){
            const worker =createWorker(URL.createObjectURL(blob));
            worker.onmessage= callback;
            workerRef.current = worker;

            return () => {
                worker.terminate();
            };
        }
    },[workerSupported])


    const createWorker = blob =>{
        var w = new Worker(blob);
        //	w.onmessage = e=> console.log('hey')
        return w;
    }

    return [
        (arg) => {
            workerRef.current.postMessage(arg);
        }
    ];
}
