import React,{useEffect,useState} from 'react';


export default  args => {
    const {ref,debug, handleOnFocus,handleOnBlur} = args;
    const [focused,setFocused] = useState(args.focused || false);

    const handleFocus = _=>{
        if(debug)
            console.log('[useFocus] focused')

        handleOnFocus && handleOnFocus();

        setFocused(true);
    }
    const handleBlur = _=>{
        if(debug)
            console.log('[useFocus] blur')

        handleOnBlur && handleOnBlur();
        
        setFocused(false);
    }
    useEffect(() => {
        if(ref && ref.current){
            if(debug)
                console.log('[useFocus] eventListener applied')


            if (!focused && document.activeElement == ref.current){
                setFocused(true);
            }
            
            ref.current.addEventListener('focus', handleFocus);
            ref.current.addEventListener('blur', handleBlur);
            return () => {
                if(ref.current){
                    ref.current.removeEventListener('focus', handleFocus)
                    ref.current.removeEventListener('blur', handleBlur)
                }
            }
        }

    }, [ref.current])

    useEffect(() => {
        if(ref && ref.current){
            focused && ref.current.focus();

         
        }

    }, [focused])


    return {hasFocus:focused,handleFocus,handleBlur};
}
